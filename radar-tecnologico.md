
# Radar Tecnológico

<br>
<br>

|                               | **Usando Ahora** | **Adoptar** | **Probar** | **Evaluar** | **Evitar** |
|:-----------------------------:|:-------------|:--------|:-------|:---------|:------|
| [**Técnicas**][tecnicas-ref]  					| <ul><li> [Integración Contínua][int-cont-ref] </li> <li> [Pequeños Lanzamientos][peq-lanz-ref] </li> <li> [Desarrollo Guiado por Prueba][tdd-ref] </li> </ul> | <ul><li>[Desarrollo Guiado por Prueba][tdd-ref]</li><li>[Programacion de a pares][pair-prog-ref]</li><li>[Modelado en Conjunto][mod-conj-ref]</li></ul> | <ul><li>[Integración Contínua][int-cont-ref]</li><li>[Participación Activa de Clientes o usuarios][active-stakehldr-part-ref]</li><li>[Testing Paralelo al Desarrollo][indp-par-test-ref]</li><li>[Pequeños Lanzamientos][peq-lanz-ref]</li><li>[Desarrollo Guiado por Características][ftr-driv-dev-ref]</li></ul> | <ul><li>[Desarrollo Guiado por Modelo Ágil][agl-model-driv-dev-ref]</li><li>[Big Design Up Front][big-dsgn-up-front-ref]</li><li>[Big Model Up Front][big-mdl-up-front-ref]</li><li>[Inspección de Código][code-inspc-ref]</li></ul> | <ul><li>[Aceptación Tradicional y Testeo Integral del Sistema][trad-accept-sys-test-ref] | 
| [**Lenguajes y Frameworks**][leng-y-frwrk-ref] 	| <ul><li>[Javascript][js-ref]</li><li>[Node][node-ref]</li><li>[Npm][npm-ref]</li><li>[MongoDB][mongo-ref]</li><li>[Electron][electron-ref]</li><li>[AngularJs][angularjs-ref]</li><li>[Java][java-ref]</li><li>[Maven][maven-ref]</li></ul> | <ul><li>[Javascript][js-ref]</li><li>[Node][node-ref]</li><li>[Npm][npm-ref]</li><li>[MongoDB][mongo-ref]</li><li>[Electron][electron-ref]</li><li>[AngularJs][angularjs-ref]</li></ul> | <ul><li>[Ionic][ionic-ref]</li><li>[React][react-ref]</li><li>[Python][python-ref]</li></ul> | <ul><li>[C#][c-sharp-ref]</li><li>[Ruby][ruby-ref]</li><li>[Scala][scala-ref]</li><li>[Swift (Apple)][swift-ref]</li><li>[Google Go][google-go-ref]</li><li>[Hack][hack-ref]</li></ul> | <ul><li>[jQuery][jquery-ref]</li></ul> |
| [**Herramientas**][herr-ref]              		| <ul><li>[Github][github-ref]</li><li>[Gitlab][gitlab-ref]</li><li>[Sublime][sublime-ref]</li><li>[Eclipse][eclipse-ref]</li><li>[Android Studio][android-st-ref]</li></ul> | <ul><li>[Github][github-ref]</li><li>[Gitlab][gitlab-ref]</li><li>[Atom][atom-ref]</li><li>[IntelliJ][intellij-ref]</li><li>[Android Studio][android-st-ref] | <ul><li>[PyCharm][pycharm-ref]</li></ul> | <ul><li>[Netbeans][netbeans-ref]</li></ul> | <ul><li>[Eclipse][eclipse-ref]</li></ul> |
| [**Plataformas**][plat-ref]      					| <ul><li>[Linux][linux-ref]</li></ul> | <ul><li>[Linux][linux-ref]</li><li>[HSTS][hsts-ref]</li></ul> | <ul><li>[Docker][docker-ref]</li></ul> | | <ul><li>[Windows][ms-windows-ref]</li></ul> |

[tecnicas-ref]:#técnicas
[leng-y-frwrk-ref]:#lenguajes-y-frameworks
[herr-ref]:#herramientas
[plat-ref]:#plataformas

[int-cont-ref]:#integración-contínua
[peq-lanz-ref]:#pequeños-lanzamientos
[tdd-ref]:#desarrollo-guiado-por-prueba
[mod-conj-ref]:#modelado-en-conjunto
[pair-prog-ref]:#programación-de-a-pares
[active-stakehldr-part-ref]:#participación-activa-de-clientes-o-usuarios
[indp-par-test-ref]:#testing-paralelo-al-desarrollo
[ftr-driv-dev-ref]:#desarrollo-guiado-por-características
[agl-model-driv-dev-ref]:#desarrollo-guiado-por-modelo-ágil
[big-dsgn-up-front-ref]:#big-design-up-front
[big-mdl-up-front-ref]:#big-model-up-front
[code-inspc-ref]:#inspección-de-código
[trad-accept-sys-test-ref]:#aceptación-tradicional-y-testeo-integral-del-sistema

[js-ref]:#javascript
[node-ref]:#node
[npm-ref]:#npm
[mongo-ref]:#mongo-db
[electron-ref]:#electron
[angularjs-ref]:#angularjs
[java-ref]:#java
[maven-ref]:#maven
[ionic-ref]:#ionic
[react-ref]:#react
[python-ref]:#python
[c-sharp-ref]:#c-sharp
[ruby-ref]:#ruby
[scala-ref]:#scala
[swift-ref]:#swift
[google-go-ref]:#go
[hack-ref]:#hack
[jquery-ref]:#jquery

[github-ref]:#github
[gitlab-ref]:#gitlab
[sublime-ref]:#sublime
[eclipse-ref]:#eclipse
[atom-ref]:#atom
[intellij-ref]:#intellij
[android-st-ref]:#android-studio
[pycharm-ref]:#pycharm
[netbeans-ref]:#netbeans

[linux-ref]:#linux
[hsts-ref]:#hsts
[docker-ref]:#docker
[ms-windows-ref]:#windows

<br>
<br>
<br>

# Técnicas

<br>

### Integración Contínua
**Continuous Integration**
* Se tiene como objetivo desarrollar, compilar el programa y probar que funcione.  
*Desventaja: potencial pérdida de tiempo dependiendo de cuán complejo sea el modelo.*  
*Posible Solución combinando* [Modelado en Conjunto][mod-conj-ref]

<br>

### Pequeños Lanzamientos
**Small Releases**
* Técnica ágil que permite cumplir progresivamente con los requerimientos planteados con un cliente o persona.

<br>

### Desarrollo Guiado por Prueba
**TDD: Test Driven Development**
* El sistema de desarrolla a partir de tests, luego se codifican los requerimientos de cada prueba.

<br>

### Modelado en Conjunto
**Model with others**
* Se tiene como objetivo desarrollar el modelo al mismo tiempo que se discute el mismo, donde todos pueden observar los cambios realizados. (Etapa previa al desarrollo)

<br>

### Programación de a Pares
**Pair Programming**
* Se trabaja de a pares en un mismo ordenador, donde uno observa lo que su compañero codifica. La ventaja es poder actuar inmediatamente ante algún imprevisto.  
*Desventaja, se cuenta con un codificador menos.*

<br>

### Participación activa de clientes o usuarios
**Active Stakeholder Participación**
* Un cliente o usuario describe específicamente algún requerimiento para luego implementar lo descripto, en un tiempo estimado en horas.

<br>

### Testing Paralelo al Desarrollo
**Independent Parallel Testing**
* Un equipo de testing trabaja independientemente del equipo de desarrollo con un lanzamiento del sistema, en etapa alfa, con el objetivo de encontrar los puntos donde el sistema no funciona correctamente, generalmente diaria o semanalmente.

<br>

### Desarrollo guiado por Características
**FDD: Feature Drive Development**
* Necesario para sistemas proyectados a gran escala. Se focaliza en la realización de procesos simples y bien definidos, es decir que cada característica debe parecerle efectiva y clara a cada miembro del equipo, mediante ciclos cortos e iterativos.

<br>

### Desarrollo guiado por modelo ágil
**AMDD: Agile Model Driven Development**
* Se exploran requerimientos con un cliente o problemas técnicos con otros desarrolladores para luego pasar directamente a resolverlos, en un tiempo aproximado/estimado en horas.

<br>

### Big Design Up Front
* Creación temprana de un documento comprensivo utilizado para guiar el modelado de un sistema.  
*Generalmente puede llevar meses o años dependiendo de cuán grande sea el proyecto.*

<br>

### Big Model Up Front
* Creación temprana de un documento comprensivo utilizado para guiar el modelado y desarrollo de un sistema.  
*Generalmente puede llevar meses o años dependiendo de cuán grande sea el proyecto.*

<br>

### Inspección de código
**Code Inspections**
* El código de un desarrollador es inspeccionado gradualmente (días, semanas, meses)  
*Sumamente efectivo en la búsqueda de 'clean code'*  
*Puede llevar mucho tiempo revisar el trabajo desarrollado de cada programador, se contrapone a las metodologías ágiles.*

<br>

### Aceptación Tradicional y Testeo Integral del Sistema
**Traditional Acceptance and System testing**
* Metodología no ágil. Se muestra por primera vez un producto casi terminado para luego comenzar las pruebas sobre éste.  
*No resulta conveniente dado que la producción pudo haber llevado meses (sino años), además de tener que realizar las pruebas de sistema necesarias en una etapa muy tardía, efecto que podría despertar resultados no esperados provocando un costo de tiempo muy alto.*

<br>
<br>

# Lenguajes y Frameworks

<br>

### Javascript
* Lenguaje fundamental en lo que respecta al desarrollo web para la implementación y uso de librerías o paquetes [Node], que utilizan [Javascript].
[Javascript]:https://www.javascript.com/ "Javascript Homepage"

<br>

### Node 
* [Node] se ha convertido en el entorno de ejecución para Javascript mas popular, liviano y eficiente.
[Node]:https://nodejs.org/en/ "Node Homepage"

<br>

### Npm
* Formando parte de [Node], [Npm] es el ecosistema de búsqueda, instalación y uso de paquetes y librerías de [Javascript] mas grande y con mayor documentación hoy en día.
[Npm]:https://www.npmjs.com/ "Npm Homepage"

<br>

### Mongo DB
* [MongoDB] es la base de datos NoSQL mas eficiente hoy en día. Provee escalabilidad a los sistemas por la simpleza al requerir un cambio de esquema en las bases de datos. Se cuenta con suficiente documentación por parte del soporte oficial y usuarios que lo utilizan.
[MongoDB]:https://www.mongodb.com/ "MongoDB Homepage"

<br>

### Electron
* [Electron] es un framework robusto que provee gran facilidad para el desarrollo de aplicaciones multiplataforma utilizando tecnologías web. Su relación con librerías y paquetes [Node] y la documentación por parte de desarrolladores y usuarios crece diaramente.
[Electron]:https://electron.atom.io/ "Electron Homepage"

<br>

### AngularJs
* Framework que proporciona robustez durante el desarrollo de SPAs en [Javascript]. Al mismo tiempo, [AngularJs] es una gran herramienta para utilizar en combinación con [Electron].
[AngularJs]:https://angularjs.org/ "AngularJs Homepage"

<br>

### Java
* Momentáneamente consideramos a [Java] el lenguaje ideal para el desarrollo de aplicaciones en proyectos [Maven], con servicios REST y Backend relativamente complejos y/o con varias estrategias de persistencia de datos.
[Java]:https://www.java.com/en/ "Java Homepage"

<br>

### Maven
* [Maven] resulta un framework ideal para programas en [Java]. Propociona un manejo de instalación/eliminación de dependencias ágil, ya sean librerías o servicios de servidor y se integra en un número aceptable de ambientes de desarrollo.
[Maven]:https://maven.apache.org/ "Maven Homepage"

<br>

### Ionic
* [Ionic] es un framework aceptable para el desarrollo de aplicaciones mobile nativas, así como también su aplicación en tecnologías web para uso Mobile. Es deseable realizar un balance costo de tiempo/calidad de los productos obtenidos con ionic en comparación con aquellos desarrollados en ambientes y lenguajes nativos, por ejemplo [Android Studio] en [Java].
[Ionic]:https://ionicframework.com/  "Ionic Homepage"

<br>

### React
* Es deseable poner en práctica [React]. Es un framework que funciona en un número aceptable de ambientes de desarrollo y permite compilar código HTML desde controladores en [Javascript] o ser referenciado desde el DOM de la aplicación. Es decir que provee rápidez y facilidad en cuanto al desarrollo Frontend en lo que respecta tecnologías web.
[React]:https://reactjs.net/ "React Homepage"

<br>

### Python
* Considerar [Python] en las tareas de lectura y escritura de archivos relativamente grandes, sobre todo en la aplicación sobre productos desarrollados en tecnología web, donde en ciertos casos la concurrencia de procesos resulta insuficiente.
[Python]:https://www.python.org/ "Python Homepage"

<br>

### C Sharp
* Hoy en día se requiere frecuentemente desarrollar productos utilizando la librería de .NET. Es necesario investigar qué ventajas traería el uso de [C#].
[C#]:https://msdn.microsoft.com/es-ar/library/kx37x362.aspx "C# Homepage"

<br>

### Ruby
* [Ruby] resulta un lenguaje sólido y expresivo en cuanto a sintaxis que ofrece la posibilidad de desarrollar en los paradigmas imperativo y funcional. Si bien su robustez nos abre las puertas a utilizarlo es necesario evaluar en qué contextos resultaría de real utilidad.
[Ruby]:https://www.ruby-lang.org/en/ "Ruby Homepage"

<br>

### Scala
* [Scala] nos ofrece desarrollar en múltiples paradigmas. Evaluar su utlización ante la necesidad de desarrollo de sistemas de contínua y gran escala, alta concurrencia de procesos, así como también en el desarrollo de grandes servidores.
[Scala]:https://www.scala-lang.org/ "Scala Homepage"

<br>

### Swift
* [Swift] es un lenguaje que a pesar de ser relativamente nuevo cuenta ya con varios proyectos en etapa de producción. Proporciona un estilo simple y expresivo para desarrollar aplicaciones para dispositivos exclusivos de Apple, ya sean para macOS, iOS, watchOS y tvOS.
[Swift]:https://developer.apple.com/swift/ "Swift Homepage"

<br>

### Go
* [Go] es un lenguaje de código abierto desarrollado por Google, con una sintáxis simple, optimizado fuertemente en lo que respecta la concurrencia de procesos. Presenta distribuciones para Linux, Mac OS X, Windows y otras.  
*Es necesario evaluar su uso práctico dado que es un lenguaje relativamente nuevo y la cantidad de librerías existentes puede no satisfacer la demanda del proyecto.*
[Go]:https://golang.org/ "Go Homepage"

<br>

### Hack
* [Hack] está pensado para potenciar el rápido desarrollo en lenguajes que exigen tipado dinámico. Generalmente orientado a aplicaciones en PHP, ya que compila en la [HHVM].
[Hack]:http://hacklang.org/ "Hack Homepage"
[HHVM]:http://hhvm.com/ "HHVM Homepage"

<br>

### JQuery
* Es de delicada importancia no aplicar funcionalidad [JQuery] directamente en aplicaciones web para evitar un fuerte acoplamiento a éste framework. Existen librerías livianas orientadas a tareas pequeñas y específicas que utilizan jQuery por detrás. De ésta manera evitamos futuros problemas de modelo.
[JQuery]:https://jquery.com/ "JQuery Homepage"

<br>
<br>

# Herramientas

<br>

### Github
* [Github] es la herramienta de control de versiones preferida momentáneamente durante la producción de proyectos públicos en equipo.
[Github]:https://github.com/ "Github Homepage"

<br>

### Gitlab
* [Gitlab] es la herramienta de control de versiones que utilizamos actualmente durante la producción de proyectos en equipo, que requieran privacidad.
[Gitlab]:https://gitlab.com/ "Gitlab Homepage"

<br>

### Sublime
* [Sublime] es un editor de texto simple y bajo consumo de recursos que posee una gran cantidad de funciones y macros que facilitan el desarrollo, además de contar con un administrador de proyectos y consola.
[Sublime]:https://www.sublimetext.com/ "Sublime Homepage"

<br>

### Eclipse
* Es un ambiente de desarrollo de código abierto, fácil uso y administración con un aceptable ecosistema de plugins de sencilla instalación. Es importante considerar la utilidad de [Eclipse] en proyectos que no requieran [Java].
[Eclipse]:https://eclipse.org/ "Eclipse Homepage"

<br>

### Atom
* Es muy recomendable comenzar a utilizar [Atom] en proyectos que requieran el uso de [Node], [Npm], y tecnologías web como [Javascript] y frameworks derivados de éste.
[Atom]:https://atom.io/ "Atom Homepage"

<br>

### IntelliJ
* Es un producto de JetBrains. Se debe tener en cuenta ésta IDE al programar en [Java]. También es considerable evaluar el uso de [IntelliJ] durante un proyecto en el que se espera desarrollar un ambiente mobile con [Android Studio] por sus similitudes gráficas y características de acceso.
[IntelliJ]:https://www.jetbrains.com/idea/ "IntelliJ Homepage"

<br>

### Android Studio
* Es de real importancia utilizar [Android Studio] al requerir producción nativa en Android.
[Android Studio]:https://developer.android.com/studio/index.html "Android Studio Homepage"

<br>

### PyCharm
* Es un producto de JetBrains. [PyCharm] es una herramienta específica para tener en cuenta al requerir producción en [Python]. La ventaja se encuentra en librerías (por ejemplo PyUnit) que funcionan considerablemente mejor que en IDES como [Eclipse].
[PyCharm]:https://www.jetbrains.com/pycharm/ "PyCharm Homepage"

<br>

### NetBeans
* Es una herramienta fuerte en cuanto a la producción de aplicaciones Web, cuenta con un servicio rest de muy fácil uso y está disponible para varias plataformas. Es necesario evaluar mas ventajas de [NetBeans].
[NetBeans]:https://netbeans.org/ "NetBeans Homepage"


<br>
<br>

# Plataformas

<br>

### Linux
* [Linux] es la plataforma actualmente preferencial para el desarrollo y entorno de prueba tanto de aplicaciones como servidores.
[Linux]:http://www.linux.org/ "Linux Homepage"

<br>

### HSTS
* [HSTS] es una política a tener en cuenta en el desarrollo de aplicaciones que manejen información sensible. Proporciona seguridad sobre las aplicaciones web, denegando el acceso de usuarios que intenten ingresar por http, forzando una conexión por https.
[HSTS]:https://www.owasp.org/index.php/HTTP_Strict_Transport_Security_Cheat_Sheet "HSTS Homepage"

<br>

### Docker
* [Docker] es una plataforma que permite utilizar frameworks, librerías o APIs de manera remota, instalándolas temporalmente en un servidor. Por ejemplo librerías de [Node], como [MongoDB], permite realizar pruebas sobre una base de datos sin necesidad de instalar el entorno en el proyecto actual. Es de real importancia cuando no se cuenta con la infraestructura necesaria que requiere la librería o API en cuestión.  
*Desventaja: fuerza el uso online de la herramienta.*
[Docker]:https://www.docker.com/ "Docker Homepage"

<br>

### Windows
* Es preferible evitar la utilización de [Windows] asi como también herramientas nativas de éste sistema operativo que puedan generar conflicto entre otros desarrolladores que se encuentren en otro entorno.  
*Apto para pruebas integrales de aplicaciones.*
[Windows]:https://www.microsoft.com/es-ar/ "MS Windows Homepage"
